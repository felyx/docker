#!/bin/sh

export DJANGO_SETTINGS_MODULE=settings
export PYTHONPATH=${PWD}

mkdir -p /opt/felyx_server/cache \
         /opt/felyx_server/cache/default \
         /opt/felyx_server/media
chmod 777 /opt/felyx_server/cache \
          /opt/felyx_server/cache/default \
          /opt/felyx_server/media

echo felyx-web-init --url ${URL} --celery-config ${CELERY_CONFIG} --results-dir ${RESULTS_DIR} --db_type ${DB_TYPE} --host ${DB_HOST} --port ${DB_PORT} --name ${DB_NAME} --user ${DB_USER} --password "${DB_PASSWORD}" ${PWD}
felyx-web-init --url ${URL} --celery-config ${CELERY_CONFIG} --results-dir ${RESULTS_DIR} \
               --db_type ${DB_TYPE} --host ${DB_HOST} --port ${DB_PORT} --name ${DB_NAME} --user ${DB_USER} --password "${DB_PASSWORD}" ${PWD}

echo felyx-web-deploy ${PWD} 
felyx-web-deploy -v ${PWD} > /opt/logs/felyx-web-deploy.log

echo felyx_django_manage collectstatic --settings ${DJANGO_SETTINGS_MODULE} --pythonpath ${PYTHONPATH} --no-input
felyx_django_manage collectstatic --settings ${DJANGO_SETTINGS_MODULE} --pythonpath ${PYTHONPATH} --no-input

sed "s/\/opt\/felyx_server\/logs/\/opt\/logs/" uwsgi.ini > /tmp/uwsgi.tmp 
cp /tmp/uwsgi.tmp uwsgi.ini 
rm /tmp/uwsgi.tmp

echo uwsgi --ini uwsgi.ini --touch-reload --immediate-uid ${UID_HOST} &
uwsgi --ini uwsgi.ini --touch-reload --immediate-uid ${UID_HOST} &

su nginx

touch ${PWD}\logs\nginx.log ${PWD}\logs\nginx.err
chmod a+rw ${PWD}\logs\nginx.log ${PWD}\logs\nginx.err

echo exec "$@"
exec "$@"

