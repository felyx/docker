# Build de l'image docker
docker build --build-arg gitlab_user="<gitlab_user>" --build-arg gitlab_passwd="<gitlab_passwd>" --build-arg gitlab_felyx_branch=master -t felyx_processor:latest .

# Lancement felyx_processor
# S'assurer avant que les repertoires a monter existent et ont tous les droits d'ecriture

# Creation du container
docker run -d --name container_felyx_processor -v /opt/felyx_processor/data:/opt/data -v /opt/felyx_processor:/opt/felyx -v /opt/felyx_processor/logs:/opt/logs -v /home/ref-copernicus-insitu:/home/ref-copernicus-insitu -v /home/datawork-cersat-public:/home/datawork-cersat-public -u 202918:11300 felyx_processor:latest

# Lancement de la commande felyx
docker exec -it container_felyx_processor felyx-extraction -p /opt/felyx/param_test.yaml -c /opt/felyx/config_new_felyx.yaml
