# Installation docker : visi-cersat-web1

# felyx tools (/export/home/python/application/felyx_tools): 
- **elasticsearch** 

Lancement : script run-elasticsearch

Configuration : 
```bash
  docker exec -it es_felyx /bin/bash
    elasticsearch-users useradd cersat_felyx_rw
      cersat_felyx_rw
      cersat_felyx_rw
    elasticsearch-users roles -a ingest_admin cersat_felyx_rw
    elasticsearch-users useradd cersat_felyx_ro
      cersat_felyx_ro
      cersat_felyx_ro
   elasticsearch-users roles -a reporting_user cersat_felyx_ro
```   

- **rabbitmq**

Lancement : script run-rabbitmq

Configuration :
```bash
  docker exec -it rabbit_felyx /bin/bash
    rabbitmqctl add_user admin admin
    rabbitmqctl set_user_tags admin administrator
    rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
    rabbitmqctl add_user felyx felyx
    rabbitmqctl set_user_tags felyx monitoring management
    rabbitmqctl add_vhost felyx_web
    rabbitmqctl set_permissions -p felyx_web admin ".*" ".*" ".*"
    rabbitmqctl set_permissions -p felyx_web felyx ".*" ".*" ".*"
    rabbitmqctl add_vhost felyx_light
    rabbitmqctl set_permissions -p felyx_light admin ".*" ".*" ".*"
    rabbitmqctl set_permissions -p felyx_light felyx ".*" ".*" ".*"
```

- **mariadb**

Lancement script run-mairadb

Configuration :
```bash
    mysql -u root -p
        Felyx2_pmt
      CREATE USER 'felyx' IDENTIFIED BY 'Felyx2_pmt';
      CREATE DATABASE felyx DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
      GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER,INDEX,REFERENCES ON felyx.* TO 'felyx';
```

# felyx configuration (/export/home/python/applications/felyx_worker) : FRONT/BACK END
Créer le répertoire de travail "WORKING_DIRECTORY" ainsi que le repertoire "logs" dans "WORKING_DIRECTORY"
```bash
./config WORKING_DIRECTORY
```
Renseigner les informations demandées
(ex : ./config /home/trash-cersat/felyx2_docker)


Vérifier "BROKER_URL" dans le fichier WORKER_DIRECTORY/conf/celery.json (le host semble KO)

# felyx serveur (/export/home/python/applications/felyx_ui): FRONT END
Vérifier le contenu de environnement.lst 

Lancement du serveur nginx et de l'application front
```bash
./run WORKING_DIRECTORY ENVIRONMENT_FILE
```
(ex : ./run /home/trash-cersat/felyx2_docker /export/home/python/applications/felyx_ui/environnement.lst)

# felyx worker (/export/home/python/applications/felyx_worker): BACK END
Lancement des workers
```bash
./run WORKING_DIRECTORY WORKER_TYPE [WORKER_ID]
```
avec WORKER_TYPE = web ou light
(ex : ./run /home/trash-cersat/felyx2_docker light)

Lancement des scripts felyx
```bash
docker exec -it worker_felyx_light SCRIPT_FELIX ARGS
```
(ex : docker exec -it worker_felyx_light felyx-diagnose --config --elasticsearch --celery --worker)