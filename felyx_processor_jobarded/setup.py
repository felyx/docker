# TODO : convert to a professional quality/production-grade source code

import click
import re
import socket
import time

@click.command()
@click.option("--mapping-tcp-host", required=True)
@click.option("--mapping-tcp-port", required=True)
def dask_setup(worker, mapping_tcp_host, mapping_tcp_port):
    r = 20
    i = 0
    f = False
    while i < r:
        client = None
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((mapping_tcp_host, int(mapping_tcp_port)))
            client.send(socket.gethostname().encode('ascii'))
            input = client.recv(1024).decode('ascii').strip()
            print('reading from socket :' + input)
            if '|' in input:
                i = r
                f = True
            else:
                time.sleep(10)
        except BaseException as e:
            print('non-fatal failed')
            print(e)
        finally:
            if client is not None:
                 client.close()
        i += 1
    if f == False:
        raise Exception('too many failed tries to get the worker mapping contact-address from the jobard remote dask')
    input_array = input.split('|')
    output = re.search(':([0-9]+)->9000', input_array[1], flags=re.IGNORECASE)
    worker.contact_address = 'tcp://'+input_array[0]+':' + str(output.group(1))
    worker.name = socket.gethostname()
    print('overriding contact-address option : ' + worker.contact_address)
