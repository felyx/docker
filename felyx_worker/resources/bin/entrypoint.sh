#!/usr/bin/env bash

if [[ ${WORKER_TYPE} == "CONFIG" ]]; then
  echo felyx configuration
  felyx-install --config --elastic --celery --plugins ${WORKER_DIR}

  exit
else

  echo felyx-install worker  ${WORKER_DIR}
  felyx-install --worker ${WORKER_DIR}

  export PATH=${WORKER_DIR}/bin:${PATH}

  if [[ ${FELYX_RECACHE} != NO ]]; then
    echo felyx-cache
    felyx-cache
  else
    if [[ ! -f /opt/felyx/workspace/local_server_cache.pickle ]]; then
      echo felyx-cache
      felyx-cache
    fi
  fi


  echo felyx-worker ${WORKER_TYPE} ${WORKER_ID}
  felyx-worker ${WORKER_TYPE} >> /opt/logs/worker_${WORKER_TYPE}${WORKER_ID}.log 2>&1

  echo exec "$@"
  exec "$@"

fi
