#!/bin/bash

docker exec -it rabbit_felyx /bin/bash 

rabbitmqctl add_user admin admin
rabbitmqctl set_user_tags admin administrator
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
rabbitmqctl add_user felyx felyx
rabbitmqctl set_user_tags felyx monitoring management
rabbitmqctl add_vhost felyx_web
rabbitmqctl set_permissions -p felyx_web admin ".*" ".*" ".*"
rabbitmqctl set_permissions -p felyx_web felyx ".*" ".*" ".*"
rabbitmqctl add_vhost felyx_light
rabbitmqctl set_permissions -p felyx_light admin ".*" ".*" ".*"
rabbitmqctl set_permissions -p felyx_light felyx ".*" ".*" ".*"

