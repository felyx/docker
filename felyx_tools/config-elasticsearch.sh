#!/bin/bash

docker exec -it es_felyx /bin/bash
  # elasticsearch-users useradd cersat_felyx_rw
  --> cersat_felyx_rw
  --> cersat_felyx_rw
  # elasticsearch-users useradd cersat_felyx_ro
  --> cersat_felyx_ro
  --> cersat_felyx_ro
  elasticsearch-users roles -a reporting_user cersat_felyx_ro
  elasticsearch-users roles -a ingest_admin cersat_felyx_rw
